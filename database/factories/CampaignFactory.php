<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Campaign;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Campaign::class, function (Faker $faker) {
    $startDate = Carbon::createFromTimeStamp($faker->dateTimeBetween('-30 days', '+30 days')->getTimestamp());
    $endDate = Carbon::createFromFormat('Y-m-d H:i:s', $startDate)->addHour();

    return [
        'name'        => $faker->name,
        'start_at'    => $startDate,
        'end_at'      => $endDate,
    ];
});
