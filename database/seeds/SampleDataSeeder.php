<?php

use App\Models\Campaign;
use Illuminate\Database\Seeder;

class SampleDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $company = \App\Models\Company::query()->create([
            'name' => 'Annie\'s Bakery Chain',
        ]);

        /** @var  \App\Models\Store $storeOfKadikoy */
        $storeOfKadikoy = \App\Models\Store::query()->create([
            'name'           => 'Kadikoy',
            'description'    => 'Annie\'s bakery chain store with a huge assortment.',
            'address'        => 'Istanbul/Turkey',
            'company_id'     => $company->id,
        ]);

        \App\Models\Store::query()->create([
            'name'           => 'Besiktas',
            'description'    => 'Annie\'s bakery chain store with a huge assortment.',
            'address'        => 'Istanbul/Turkey',
            'company_id'     => $company->id,
        ]);

        $campaign = Campaign::query()->create([
            'name'                       => 'Cupcakes',
            'total_product_limit'        => 100,
            'product_limit_per_user'     => 5,
            'start_at'                   => now(),
            'end_at'                     => now()->addDays(7),
        ]);

        /** @var \App\Models\Product $product */
        $product = \App\Models\Product::query()->create([
            'name'     => 'Cupcake',
        ]);

        $product->campaigns()->sync([$campaign->id]);

        $product->stores()->sync([$storeOfKadikoy->id]);
    }
}
