## About Bakery Chain

Annie’s is a very known bakery chain in the city and is planning to introduce a new product,
cupcakes. As part of the launch campaign, they are planning to give away free samples during the
launch week to any customer that registers on the store’s website.

## Installation
```bash
git clone https://Mus7afa@bitbucket.org/Mus7afa/bakery.git
composer install
copy .env.example file to .env
php artisan key:generate
php artisan create:admin
php artisan db:seed --class=SampleDataSeeder
```

