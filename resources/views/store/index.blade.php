@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <a href="{{ route('stores.create') }}" class="btn btn-primary float-right mb-3" role="button">Create Store</a>

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Company</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col" class="text-center">Operation</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stores as $store)
                        <tr>
                            <th class="align-middle" scope="row">{{ $store->id }}</th>
                            <td class="align-middle">{{ $store->name }}</td>
                            <td class="align-middle">{{ $store->address }}</td>
                            <td class="align-middle"><a href="{{ route('companies.edit', ['id' => $store->company->id ]) }}">{{ $store->company->name }}</a></td>
                            <td class="align-middle">{{ $store->created_at }}</td>
                            <td class="align-middle">{{ $store->updated_at }}</td>
                            <td class="align-middle text-center">
                                <a href="{{ route('stores.edit', ['id' => $store->id ]) }}" class="btn btn-success" role="button">Edit</a>
                                <form action="{{route('stores.destroy',['id' => $store->id ])}}" method="POST" style="display: inline;">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $stores->links() }}
            </div>
        </div>
    </div>
@endsection
