@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <form method="POST" action="{{ route('stores.update', ['id' => $store->id]) }}">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="nameInput">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $store->name }}" id="nameInput" placeholder="Enter Name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control @error('description') is-invalid @enderror" rows="4" name="description" id="description">{{ $store->description }}</textarea>
                        @error('description')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="address">Address</label>
                        <textarea class="form-control @error('address') is-invalid @enderror" rows="2" name="address" id="address">{{ $store->address }}</textarea>
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="company">Company</label>
                        <select class="form-control @error('company_id') is-invalid @enderror" name="company_id" id="company">
                            <option value="">--Select Company--</option>
                            @foreach($companies as $company)
                                <option value="{{ $company->id }}" @if ($company->id === $store->company_id) selected="selected" @endif>{{ $company->name }}</option>
                            @endforeach
                        </select>
                        @error('company_id')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection













