@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-3">
        @foreach($products as $product)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $product->name }}</h5>
                        <img class="card-img" src="https://via.placeholder.com/90" alt="" />
                        <p class="card-text">{{ $product->description ?? '' }}</p>
                        <div>
                            <a href="{{ route('detail', ['id' => $product->id ]) }}" class="btn btn-primary float-right">Go Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            @if ($loop->iteration % 3 === 0)
                </div>
                <div class="row mt-3">
            @endif
        @endforeach
    </div>
    {{ $products->links() }}
</div>
@endsection


