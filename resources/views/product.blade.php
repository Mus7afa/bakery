@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">Product Name: {{ $product->name }}</div>
                    <div class="card-body">
                        <img class="card-img" src="https://via.placeholder.com/250"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <form method="POST" action="{{ route('orders.store', ['product' => $product->id]) }}">
                    @csrf
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" pattern="\d*" class="form-control @error('amount') is-invalid @enderror"
                               name="amount" value="{{ old('amount') }}" id="amount" placeholder="Enter Amount">
                        @error('amount')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="picked_at">Pick Date</label>
                        <input type="date" class="form-control @error('picked_at') is-invalid @enderror"
                               name="picked_at" value="{{ old('picked_at') }}" id="picked_at" placeholder="Select Date">
                        @error('picked_at')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="campaign">Available Campaigns</label>
                        <select class="form-control" name="campaign_id" id="campaign">
                            <option></option>
                            @foreach($product->campaigns as $campaign)
                                <option value="{{ $campaign->id }}"> {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="store">Which Stores</label>
                        <select class="form-control" name="store_id" id="store">
                            <option></option>
                            @foreach($product->stores as $store)
                                <option value="{{ $store->id }}"> {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary float-right ml-2">Get Free Products</button>

                        <button class="btn btn-primary disabled float-right">Add Basket</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


