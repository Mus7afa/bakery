@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <form method="POST" action="{{ route('products.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="nameInput">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" id="nameInput" placeholder="Enter Name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="campaigns">Campaigns</label>
                        <select multiple class="form-control" name="campaigns[]" id="campaigns">
                            <option></option>
                            @foreach($campaigns as $campaign)
                                <option value="{{ $campaign->id }}"> {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="stores">Stores</label>
                        <select multiple class="form-control" name="stores[]" id="stores">
                            <option></option>
                            @foreach($stores as $store)
                                <option value="{{ $store->id }}"> {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>


                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
@endsection

