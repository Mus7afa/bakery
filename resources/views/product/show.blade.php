@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <h5 class="card-header">{{ $product->name }}</h5>
                    <div class="card-body">
                        <p class="card-text">
                        <table>
                            <tr>
                                <th class="text-muted pr-2" scope="row">Name</th>
                                <td></td>
                                <td>{{ $product->name }}</td>
                            </tr>
                            <tr>
                                <th class="text-muted pr-2" scope="row">Created At</th>
                                <td></td>
                                <td>{{ $product->created_at }}</td>
                            </tr>
                            <tr>
                                <th class="text-muted pr-2" scope="row">Updated At</th>
                                <td></td>
                                <td>{{ $product->updated_at }}</td>
                            </tr>
                            <tr>
                                <th class="text-muted pr-2" scope="row">Campaigns</th>
                                <td></td>
                                <td>
                                    @foreach($product->campaigns as $campaign)
                                        <a href="{{ route('campaigns.edit', ['id' => $campaign->id]) }}" title="{{ $campaign->name }}"> {{ $campaign->name }}</a>  @if (!$loop->last) | @endif
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th class="text-muted pr-2" scope="row">Stores</th>
                                <td></td>
                                <td>
                                    @foreach($product->stores as $store)
                                        <a href="{{ route('stores.edit', ['id' => $store->id]) }}" title="{{ $store->name }}"> {{ $store->name }}</a> @if (!$loop->last) | @endif
                                    @endforeach
                                </td>
                            </tr>
                        </table>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
