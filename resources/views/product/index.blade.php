@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <a href="{{ route('products.create') }}" class="btn btn-primary float-right mb-3" role="button">Create Product</a>

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col" class="text-center">Campaigns</th>
                        <th scope="col" class="text-center">Stores</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col" class="text-center">Operation</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($products as $product)
                        <tr>
                            <th class="align-middle" scope="row">{{ $product->id }}</th>
                            <td class="align-middle">{{ $product->name }}</td>
                            <td class="align-middle text-center"><a href="{{ route('campaigns.index', ['product_id' => $product->id]) }}">{{ $product->campaigns_count }}</a></td>
                            <td class="align-middle text-center"><a href="{{ route('stores.index', ['product_id' => $product->id]) }}">{{ $product->stores_count }}</a></td>
                            <td class="align-middle">{{ $product->created_at }}</td>
                            <td class="align-middle">{{ $product->updated_at }}</td>
                            <td class="align-middle text-center">
                                <a href="{{ route('products.show', ['id' => $product->id ]) }}" class="btn btn-primary" role="button">Show</a>

                                <a href="{{ route('products.edit', ['id' => $product->id ]) }}" class="btn btn-success" role="button">Edit</a>

                                <form action="{{route('products.destroy',['id' => $product->id])}}" method="POST" style="display: inline;">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $products->links() }}
            </div>
        </div>
    </div>
@endsection
