@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <form method="POST" action="{{ route('products.update', ['id' => $product->id]) }}">
                    @method('PUT')
                    @csrf
                    <div class="form-group">
                        <label for="nameInput">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $product->name }}" id="nameInput" placeholder="Enter Name">
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="campaigns">Campaigns</label>
                        <select multiple class="form-control" name="campaigns[]" id="campaigns">
                            <option></option>
                            @foreach($product->campaigns as $productCampaign)
                                <option value="{{ $productCampaign->id }}" selected> {{ $productCampaign->name }}</option>
                            @endforeach
                            @foreach($campaigns as $campaign)
                                <option value="{{ $campaign->id }}"> {{ $campaign->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="stores">Stores</label>
                        <select multiple class="form-control" name="stores[]" id="stores">
                            <option></option>
                            @foreach($product->stores as $productStore)
                                <option value="{{ $productStore->id }}" selected> {{ $productStore->name }}</option>
                            @endforeach
                            @foreach($stores as $store)
                                <option value="{{ $store->id }}"> {{ $store->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
@endsection
