@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <a href="{{ route('companies.create') }}" class="btn btn-primary float-right mb-3" role="button">Create Company</a>

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col" class="text-center">Operation</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($companies as $company)
                        <tr>
                            <th class="align-middle" scope="row">{{ $company->id }}</th>
                            <td class="align-middle">{{ $company->name }}</td>
                            <td class="align-middle">{{ $company->created_at }}</td>
                            <td class="align-middle">{{ $company->updated_at }}</td>
                            <td class="align-middle text-center">
                                <a href="{{ route('companies.edit', ['id' => $company->id ]) }}" class="btn btn-success" role="button">Edit</a>

                                <form action="{{route('companies.destroy',[$company->id])}}" method="POST" style="display: inline;">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $companies->links() }}
            </div>
        </div>
    </div>
@endsection
