@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Campaign</th>
                        <th scope="col">Store</th>
                        <th scope="col">Product</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Picked At</th>
                        @role('admin')
                            <th scope="col" class="text-center">Operation</th>
                        @endrole
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <th class="align-middle" scope="row">{{ $order->id }}</th>
                            <td class="align-middle">{{ $order->user->name }}</td>
                            <td class="align-middle">{{ $order->campaign->name }}</td>
                            <td class="align-middle">{{ $order->store->name }}</td>
                            <td class="align-middle">{{ $order->product->name }}</td>
                            <td class="align-middle">{{ $order->amount }}</td>
                            <td class="align-middle">{{ $order->picked_at }}</td>
                            @role('admin')
                                <td class="align-middle text-center">
                                    <form action="{{route('orders.destroy',[$order->id])}}" method="POST" style="display: inline;">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            @endrole
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $orders->links() }}
            </div>
        </div>
    </div>
@endsection
