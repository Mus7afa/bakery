@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <a href="{{ route('campaigns.create') }}" class="btn btn-primary float-right mb-3" role="button">Create Campaign</a>

                <table class="table">
                    <thead class="thead-light">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Start At</th>
                        <th scope="col">End At</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Updated At</th>
                        <th scope="col" class="text-center">Operation</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($campaigns as $campaign)
                        <tr>
                            <th class="align-middle" scope="row">{{ $campaign->id }}</th>
                            <td class="align-middle">{{ $campaign->name }}</td>
                            <td class="align-middle">{{ $campaign->start_at }}</td>
                            <td class="align-middle">{{ $campaign->end_at }}</td>
                            <td class="align-middle">{{ $campaign->created_at }}</td>
                            <td class="align-middle">{{ $campaign->updated_at }}</td>
                            <td class="align-middle text-center">
                                <a href="{{ route('campaigns.edit', ['id' => $campaign->id ]) }}" class="btn btn-success" role="button">Edit</a>

                                <form action="{{route('campaigns.destroy',[$campaign->id])}}" method="POST" style="display: inline;">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {{ $campaigns->links() }}
            </div>
        </div>
    </div>
@endsection
