@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <form method="POST" action="{{ route('campaigns.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="nameInput">Name</label>
                        <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" id="nameInput" placeholder="Enter Name">
                        @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="totalProductLimit">Total Product Limit</label>
                        <input type="number" pattern="\d*"  class="form-control @error('total_product_limit') is-invalid @enderror" value="{{ old('total_product_limit') }}" name="total_product_limit" id="totalProductLimit" placeholder="Enter Total Product Limit">
                        @error('total_product_limit')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="productLimitPerUser">Product Limit Per User</label>
                        <input type="number" pattern="\d*" class="form-control @error('product_limit_per_user') is-invalid @enderror" value="{{ old('product_limit_per_user') }}" name="product_limit_per_user" id="productLimitPerUser" placeholder="Enter Product Limit Per User">
                        @error('product_limit_per_user')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="startAt">Start At</label>
                        <input type="text" class="form-control @error('start_at') is-invalid @enderror" value="{{ old('start_at') }}" name="start_at" id="startAt" placeholder="Enter Start Date, Ex: 2018-11-20">
                        @error('start_at')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="endAt">End At</label>
                        <input type="text" class="form-control @error('end_at') is-invalid @enderror" value="{{ old('end_at') }}" name="end_at" id="endAt" placeholder="Enter End Date, Ex: 2018-11-27">
                        @error('end_at')
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function($){
            $('#startAt').mask('0000-00-00');
            $('#endAt').mask('0000-00-00');
        });
    </script>
@endsection
