<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int|null total_product_limit
 * @property int|null product_limit_per_user
 * @property int id
 */
class Campaign extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'total_product_limit',
        'product_limit_per_user',
        'start_at',
        'end_at',
    ];

    /**
     * The stores that belong to the campaign.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function stores()
    {
        return $this->belongsToMany(Store::class);
    }

    /**
     * The products that belong to the campaign.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    /**
     * Get the orders for the campaign.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    /**
     * @return int
     */
    public function totalOrderedProductCount(): int
    {
        return (int) $this->orders()->sum('amount');
    }
}
