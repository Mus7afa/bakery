<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Spatie\Permission\Models\Role;

class CreateAdminCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the admin user.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->ask('User name');
        $email = $this->ask('User email');
        $password = $this->secret('What is the password?');

        if (!$this->confirm('Do you wish to continue?')) {
            $this->info('Canceled.');
        }

        /** @var User $user */
        $user = User::query()->firstOrCreate([
            'email'    => $email,
        ], [
            'name'     => $name,
            'password' => bcrypt($password),
        ]);

        $role = Role::query()->firstOrCreate([
            'name' => 'admin',
        ]);

        $user->assignRole($role->name);
    }
}
