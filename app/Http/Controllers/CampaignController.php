<?php

namespace App\Http\Controllers;

use App\Models\Campaign;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Campaign\StoreCampaignRequest;
use App\Http\Requests\Campaign\UpdateCampaignRequest;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $campaignQuery = Campaign::query();

        $productId = $request->input('product_id');
        $campaignQuery->when($productId, function (Builder $q) use ($productId) {
            $q->whereHas('products', function (Builder $q) use ($productId) {
                $q->where('id', $productId);
            });
        });

        $campaigns = $campaignQuery->paginate();

        return view('campaign.index', compact('campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Campaign\StoreCampaignRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCampaignRequest $request)
    {
        $campaignAttributes = [
            'name'                       => $request->input('name'),
            'total_product_limit'        => $request->input('total_product_limit'),
            'product_limit_per_user'     => $request->input('product_limit_per_user'),
            'start_at'                   => $request->input('start_at'),
            'end_at'                     => $request->input('end_at'),
        ];

        Campaign::query()->create($campaignAttributes);

        return redirect()->route('campaigns.index')->with('success', 'The campaign has successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $campaign = Campaign::query()->findOrFail($id);

        return view('campaign.show', compact('campaign'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $campaign = Campaign::query()->findOrFail($id);

        return view('campaign.edit', compact('campaign'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Campaign\UpdateCampaignRequest $request
     * @param int                                               $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCampaignRequest $request, int $id)
    {
        $campaign = Campaign::query()->findOrFail($id);

        $campaignAttributes = [
            'name'                       => $request->input('name'),
            'total_product_limit'        => $request->input('total_product_limit'),
            'product_limit_per_user'     => $request->input('product_limit_per_user'),
            'start_at'                   => $request->input('start_at'),
            'end_at'                     => $request->input('end_at'),
        ];

        $campaign->update($campaignAttributes);

        return redirect()->route('campaigns.index')->with('success', 'The campaign has successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $campaign = Campaign::query()->findOrFail($id);

        $campaign->delete();

        return redirect()->route('campaigns.index')->with('success', "The campaign [{$campaign->name}] has successfully deleted.");
    }
}
