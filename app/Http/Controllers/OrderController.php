<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Campaign;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Order\StoreOrderRequest;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orderQuery = Order::query()->with(['user', 'product', 'campaign', 'store']);

        /** @var User $authUser */
        $authUser = auth()->user();
        if (! $authUser->hasRole('admin')) {
            $orderQuery->whereHas('user', function (Builder $q) {
                $q->where('id', auth()->id());
            });
        }

        $orders = $orderQuery->paginate();

        return view('order.index', compact('orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Order\StoreOrderRequest $request
     * @param int                                        $productId
     *
     * @throws \Illuminate\Validation\ValidationException
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreOrderRequest $request, int $productId)
    {
        /** @var Product $product */
        $product = Product::query()->findOrFail($productId);

        $campaignId = $request->input('campaign_id');
        $storeId = $request->input('store_id');
        $amount = $request->input('amount');
        $pickedAt = $request->input('picked_at');

        /** @var User $authUser */
        $authUser = auth()->user();

        /** @var Campaign $campaign */
        $campaign = Campaign::query()->findOrFail($campaignId);

        if($validationMessage = $this->validateOrder($campaign, $authUser, $amount)) {
            return redirect()->back()->with('warning', $validationMessage);
        }

        $orderAttributes = [
            'user_id'     => $authUser->getAuthIdentifier(),
            'campaign_id' => $campaignId,
            'store_id'    => $storeId,
            'picked_at'   => $pickedAt,
            'product_id'  => $product->id,
            'amount'      => $amount,
        ];

        Order::query()->create($orderAttributes);

        return redirect()->route('home')->with('success', "{$amount} products were ordered.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(int $id)
    {
        $order = Order::query()->findOrFail($id);

        $order->delete();

        return redirect()->route('products.index')->with('success', 'The order has successfully deleted.');
    }

    private function validateOrder(Campaign $campaign, User $user, int $amount)
    {
        if ($campaign->totalOrderedProductCount() >= $campaign->total_product_limit) {

            return 'The Campaign limit exceeded. Thanks for your interest.';
        }

        $authUserTotalOrderedProductCount = $user->totalOrderedProductCountIn($campaign);
        if ($authUserTotalOrderedProductCount >= $campaign->product_limit_per_user) {

            return 'You have exceeded the limit of the number of products that can order.';
        }

        $authUserTotalOrderAmount = $amount + $authUserTotalOrderedProductCount;
        if ($authUserTotalOrderAmount > $campaign->product_limit_per_user) {
            $remainingOrderCount = $campaign->product_limit_per_user - $authUserTotalOrderedProductCount;

            return "You cannot order {$amount} products. You have the right to {$remainingOrderCount} products.";
        }

        return null;
    }
}
