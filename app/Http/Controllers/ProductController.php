<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Product;
use App\Models\Campaign;
use Illuminate\Http\Request;
use App\Http\Requests\Product\StoreProductRequest;
use App\Http\Requests\Product\UpdateProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::query()->withCount(['campaigns', 'stores'])->paginate();

        return view('product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // @TODO we can use autocomplete in view.
        $campaigns = Campaign::query()->limit(10)->latest()->get();

        // @TODO we can use autocomplete in view.
        $stores = Store::query()->limit(10)->latest()->get();

        return view('product.create', compact('campaigns', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Product\StoreProductRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        /** @var Product $product */
        $product = Product::query()->create([
            'name'     => $request->input('name'),
        ]);

        $campaigns = array_filter($request->input('campaigns', []));
        $product->campaigns()->sync($campaigns);

        $stores = $request->input('stores', []);
        $product->stores()->sync($stores);

        return redirect()->route('products.index')->with('success', 'The product has successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = Product::query()->with('campaigns', 'stores')->findOrFail($id);

        return view('product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $product = Product::query()->with('campaigns', 'stores')->findOrFail($id);

        $currentCampaigns = $product->campaigns->pluck('id');

        // @TODO we can use autocomplete in view.
        $campaigns = Campaign::query()->limit(10)->whereNotIn('id', $currentCampaigns)->latest()->get();

        $currentStores = $product->stores->pluck('id');
        // @TODO we can use autocomplete in view.
        $stores = Store::query()->limit(10)->whereNotIn('id', $currentStores)->latest()->get();

        return view('product.edit', compact('product', 'campaigns', 'stores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Product\UpdateProductRequest $request
     * @param int                                             $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, int $id)
    {
        /** @var Product $product */
        $product = Product::query()->findOrFail($id);

        $product->update([
            'name'     => $request->input('name'),
        ]);

        $campaigns = array_filter($request->input('campaigns', []));
        $product->campaigns()->sync($campaigns);

        $stores = $request->input('stores', []);
        $product->stores()->sync($stores);

        return redirect()->route('products.index')->with('success', 'The product has successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $product = Product::query()->findOrFail($id);

        $product->delete();

        return redirect()->route('products.index')->with('success', "The product [{$product->name}] has successfully deleted.");
    }
}
