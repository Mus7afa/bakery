<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ProductDetailController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $product = Product::query()
            ->with([
                'campaigns' => function (BelongsToMany $q) {
                    $q->whereDate('start_at', '>=', now());
                },
                'stores',
            ])
            ->findOrFail($id);

        return view('product', compact('product'));
    }
}
