<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\Store\StoreRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Store\UpdateStoreRequest;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $storeQuery = Store::query()->with('company');

        $productId = $request->input('product_id');
        $storeQuery->when($productId, function (Builder $q) use ($productId) {
            $q->whereHas('products', function (Builder $q) use ($productId) {
                $q->where('id', $productId);
            });
        });

        $stores = $storeQuery->paginate();

        return view('store.index', compact('stores'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // @TODO we can use autocomplete in view.
        $companies = Company::query()->limit(10)->latest()->get();

        return view('store.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Store\StoreRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        Store::query()->create([
            'name'           => $request->input('name'),
            'description'    => $request->input('description'),
            'address'        => $request->input('address'),
            'company_id'     => $request->input('company_id'),
        ]);

        return redirect()->route('stores.index')->with('success', 'The store has successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $store = Store::query()->findOrFail($id);

        return view('store.show', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $store = Store::query()->findOrFail($id);

        // @TODO we can use autocomplete in view.
        $companies = Company::query()->limit(10)->latest()->get();

        return view('store.edit', compact('store', 'companies'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Store\UpdateStoreRequest $request
     * @param int                                         $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStoreRequest $request, int $id)
    {
        /** @var Store $store */
        $store = Store::query()->findOrFail($id);

        $store->update([
            'name'           => $request->input('name'),
            'description'    => $request->input('description'),
            'address'        => $request->input('address'),
            'company_id'     => $request->input('company_id'),
        ]);

        return redirect()->route('stores.index')->with('success', 'The store has successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $store = Store::query()->findOrFail($id);

        $store->delete();

        return redirect()->route('stores.index')->with('success', "The store [{$store->name}] has successfully updated.");
    }
}
