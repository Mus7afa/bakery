<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use App\Http\Requests\Company\StoreCompanyRequest;
use App\Http\Requests\Company\UpdateCompanyRequest;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::query()->paginate();

        return view('company.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Company\StoreCompanyRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {
        Company::query()->create([
            'name' => $request->input('name'),
        ]);

        return redirect()->route('companies.index')->with('success', 'The company has successfully saved.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $company = Company::query()->findOrFail($id);

        return view('company.show', compact('company'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::query()->findOrFail($id);

        return view('company.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Company\UpdateCompanyRequest $request
     * @param int                                             $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCompanyRequest $request, int $id)
    {
        $company = Company::query()->findOrFail($id);

        $company->update([
            'name' => $request->input('name'),
        ]);

        return redirect()->route('companies.index')->with('success', 'The company has successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $company = Company::query()->findOrFail($id);

        $company->delete();

        return redirect()->route('companies.index')->with('success', "The company [{$company->name}] has successfully updated.");
    }
}
