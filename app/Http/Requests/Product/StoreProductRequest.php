<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|string|min:3|max:255',
            'campaigns'   => 'nullable|array',
            'campaigns.*' => 'nullable|int|exists:campaigns,id',
            'stores'      => 'required|array',
            'stores.*'    => 'required|int|exists:stores,id',
        ];
    }
}
