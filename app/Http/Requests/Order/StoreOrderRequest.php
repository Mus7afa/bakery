<?php

namespace App\Http\Requests\Order;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picked_at'      => 'required|date|date_format:Y-m-d',
            'amount'         => 'required|int|min:1',
            'store_id'       => 'required|int|exists:stores,id',
            'campaign_id'    => 'required|int|exists:campaigns,id',
        ];
    }
}
