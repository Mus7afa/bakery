<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/products/{id}/detail', 'ProductDetailController@show')->name('detail');

Route::middleware(['auth'])->group(function () {
    Route::post('product/{product}/orders', 'OrderController@store')->name('orders.store');
    Route::resource('orders', 'OrderController')->only(['index', 'destroy']);
});

Route::middleware(['auth', 'role:admin'])->group(function () {
    Route::get('users', 'UserController@index')->name('users.index');
    Route::resource('companies', 'CompanyController');
    Route::resource('stores', 'StoreController');
    Route::resource('products', 'ProductController');
    Route::resource('campaigns', 'CampaignController');
});



